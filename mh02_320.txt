;0
__main

;7
@Lclearscreenall

;24
@ネロのキーヒント

;50
OPR0203410

;61
Hello, Nero.

;80
NER0200830

;91
Hm?

;98
OPR0203420

;109
Do you think this scene is suspicious?

;164
NER0200840

;175
Yeah, I can feel it.

;203
NER0200850

;214
What about you, Kobayashi?
Did something catch your attention?

;278
OPR0203430

;289
Let's see...

;308
NER0200890

;319
Hey.

;329
OPR0203440

;340
W-What...?

;356
NER0200900

;367
There wasn't any problem with that point you
just showed me. You ended up wasting time.

;483
OPR0203450

;494
Is that so... then...

;525
2001

;530
NER0200860

;541
There? Hmm... Then I'll go check it out...

;612
NER0200910

;623
This guy is suspicious, isn't he?

;654
OPR0203460

;665
Now that you mention it, he's taking a bunch
of pictures, and writing stuff in his PDA...

;742
OPR0203470

;753
His suit is pretty casual too...
He doesn't look like a crime scene
inspector from the police...

;836
OPR0203480

;847
I wonder who he is?

;872
NER0200920

;883
He's an examiner from the insurance company.

;920
OPR0203490

;931
Eh, is he?

;959
NER0200930

;970
I tried talking to him earlier.

;1010
NER0200940

;1021
I wanted to know what he was writing down
so I said "Show me your PDA".

;1101
NER0200950

;1112
And so he said
"Don't be ridiculous!" and got mad at me.

;1180
OPR0203500

;1191
Of course he did...

;1222
NER0200960

;1233
Ah...!

;1243
OPR0203510

;1254
Eh?

;1261
NER0200970

;1272
Hehehe～ Kobayashi～...

;1303
OPR0203520

;1314
W-What is it?

;1336
NER0200980

;1347
Cooperate with me a bit.

;1378
OPR0203530

;1389
Cooperate...?

;1402
............

;1415
NER0200990

;1426
Hey, hey.

;1442
ISR0200010

;1453
You again...

;1475
ISR0200020

;1486
No matter how many times you ask I won't
show you my PDA. I could get fired.

;1578
NER0201000

;1589
No matter what?

;1608
ISR0200030

;1619
No matter what.

;1638
NER0201010

;1649
Cheapskate.

;1659
ISR0200040

;1670
Listen, it's not about...

;1710
OPR0203540

;1721
Oops! Excuse me!

;1746
ISR0200050

;1757
Wa!

;1764
NER0201020

;1775
Woah!

;1788
OPR0203550

;1799
I'm sorry...
I wasn't looking where I was going...

;1867
ISR0200060

;1878
Hey, be careful～

;1933
OPR0203560

;1944
I'm sorry, I'm sorry.

;1981
NER0201030

;1992
Hey, here.

;2011
ISR0200070

;2022
Ah!

;2029
ISR0200080

;2040
You didn't look right!?

;2071
NER0201040

;2082
How would I have seen in such
a short amount of time?

;2143
ISR0200090

;2154
Geeze...!

;2173
NER0201050

;2184
Hehehe...

;2200
OPR0203580

;2211
Did you read it?

;2233
NER0201060

;2244
Perfectly!

;2260
NER0201070

;2271
But, it's not that I "understand" the contents,
I just "poured" it into my PDA.

;2375
OPR0203590

;2386
So you copied it?

;2408
NER0201080

;2419
Right, if I can touch it,
it only takes an instant.

;2480
OPR0203600

;2491
That's really something.

;2519
NER0201090

;2530
Now then, the contents...

;2564
NER0201100

;2575
――――

;2588
OPR0203610

;2599
How is it?

;2615
NER0201110

;2626
Oh...nI can't overlook this...

;2678
OPR0203620

;2689
There's something there.

;2720
NER0201120

;2731
Here, look at this data item...

;2771
OPR0203630

;2782
Hmm...

;2795
OPR0203650

;2806
!

;2810
NER0201130

;2821
Right, this is an important clue...

;2873
OPR0203660

;2884
Hey hey, that's my line...

;2927
NER0201140

;2938
Hehehe, I wanted to try saying it once.

;2996
OPR0203670

;3007
@カードの秘密

