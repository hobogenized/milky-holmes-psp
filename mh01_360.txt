;0
__main

;7
@Lclearscreenall

;24
@010401_コーデリアのキーヒント

;66
CDR0102060

;77
Ah, Mr Kobayashi.

;99
OPR0105290

;110
Miss Cordelia.

;144
CDR0102070

;155
That's too much... Cordelia is fine..

;207
OPR0105300

;218
Yeah?

;228
CDR0102080

;239
But to think the Mr Kobayashi is here in
front of me... I'm kind of nervous...

;328
OPR0105310

;339
Ha ha...
I-Is there something around here
that caught your attention?

;397
CDR0102090

;408
Yes... I think it's suspicious.

;454
CDR0102100

;465
Is there anything you notice, Mr Kobayashi?

;514
OPR0105320

;525
Let's see...

;544
CDR0102130

;555
Um, about the place you just pointed out...
I didn't find anything particularly wrong...

;668
OPR0105330

;679
Is that so... then...

;710
1003

;715
CDR0102110

;726
Alright, I'll thoroughly examine there.

;787
CDR0102140

;798
That's suspicious, isn't it...

;835
OPR0105340

;846
Indeed...

;871
............

;884
OPR0105360

;895
Mr Tachi!

;908
TCH0100970

;919
What is it?

;941
OPR0105370

;952
Is this Miss Henriette's cellphone?

;1007
TCH0100980

;1018
Yes, it is...

;1049
OPR0105380

;1060
Why is it laying here?

;1100
CDR0102150

;1111
Wouldn't it be because
there was a struggle with the intruder?

;1185
OPR0105390

;1196
It's true that it may have
tumbled over here at that time, but...

;1270
OPR0105400

;1281
To be right in front of the door...

;1324
OPR0105410

;1335
Hm?

;1342
CDR0102160

;1353
What is it?

;1375
OPR0105420

;1386
It says that there is a missed call...
Was it like this from the start?

;1460
CDR0102170

;1471
Yes.

;1481
OPR0105430

;1492
Hmm～, I'd like to check it out,
but we can't go about touching it...

;1572
CDR0102180

;1583
You wouldn't be able to, even if you could touch it.

;1629
OPR0105440

;1640
Eh?

;1647
CDR0102190

;1658
Look, a key mark, in other words, it's been locked.

;1729
OPR0105450

;1740
Locked...?

;1765
CDR0102200

;1776
?

;1780
OPR0105460

;1791
Cordelia.

;1810
CDR0102210

;1821
Yes?

;1831
OPR0105470

;1842
When you leave your phone within reach,
do you lock it?

;1931
CDR0102220

;1942
No, otherwise I wouldn't be able to quickly...

;1976
CDR0102230

;1987
Ah!

;1994
OPR0105480

;2005
Right, this is an important clue...

;2057
@謎の怪盗

