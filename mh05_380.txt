;0
__main

;7
@Lclearscreenall

;24
@5ココロ

;36
AKT0500900

;47
#絶対、小衣が手がかりを見つけるんだから…
Kokoro will definitely find every clue...

;108
AKT0500910

;119
#怪盗なんかすぐにつかまえて…
#警視をいじめたオヤジなんか見返して…
I'll catch the Phantom Thieves in no time...
I'll show them, those geezers who bullied Chief...

;217
OPR0503510

;228
#あの…
Excuse me...

;238
AKT0500920

;249
#なによ！
What?!

;262
OPR0503520

;273
#いや…その…
No... It's just...

;292
OPR0503530

;303
#なにか、困ってることはないかなって…
I was wondering if something was bothering you...

;358
AKT0500930

;369
#そんなのあるわけないでしょ！
#小衣は天才なのよ！
Of course not!
Kokoro is a genius after all!

;440
OPR0503540

;451
#いや…ほら…なれない場所だからさ…。
No, look, maybe it's because
you are in an unfamiliar place...

;506
AKT0500940

;517
#よけいなお世話ね！
#今は仕方なくここにいるんだから！
None of your business!
I'm here because I have no other choice!

;594
OPR0503550

;605
#まぁまぁ…そんなこと言わないで…
#仲良くやっていこうよ…
Come on, don't say that...
Let's try to get along...

;688
OPR0503560

;699
#探偵学院と警察学校の子を集めて
#パーティもやるみたいだし…
There will even be a party for students
from both the Detective and the Police academies...

;785
AKT0500950

;796
#む！？
Uh?!

;806
OPR0503570

;817
#え？
Eh?

;824
AKT0500960

;835
#なにが…なにがパーティよ…
So... So what!?...

;875
AKT0500970

;886
#警察と探偵がパーティなんて…
A party for detectives and the police...

;929
AKT0500980

;940
#しかも、なんでその警備を
#小衣たちがやらなきゃいけないの！
Also, why does the G4 has to be
in charge of the security there?!

;1026
SRK0501220

;1037
#え！？　ココロちゃんたちも来るの！？
Eh?! Kokoro-chan will be there too?!

;1092
AKT0500990

;1103
#不本意ながらね！
Only because I have to!

;1128
SRK0501230

;1139
#やった～！　わ～い！
Yay～! Wai～!

;1170
OPR0503580

;1181
#ん…？
Hm...?

;1191
#【G4が会場警備役に任命された】
#～この疑問点を覚えますか？～
【G4 is taking care of the security】
～Is this suspicious?～

;1279
AKT0501000

;1290
#今は一刻も早く
#怪盗をつかまえないといけないのに…
Even though, right now, our top priority
should be capturing the phantom thieves...

;1364
AKT0501010

;1375
#みんなバカばっかりなんだから！
Everyone is so foolish!

;1421
…………

;1434
AKT0501020

;1445
#いつまで見てんの！？
How long have you been watching?!

;1476
OPR0503600

;1487
#あ、いや…。
Ah, no...

;1506
AKT0501030

;1517
#う～…！
Uu～...!

;1530
OPR0503610

;1541
#これ以上は話しかけないでおこう…。
Let's not bother her any further...

;1593
@捜査の後

