;0
__main

;7
@Lclearscreenall

;24
@シャロとデート

;47
OPR0600090

;58
#事件解決のごほうび…
The prize for solving the case...

;89
OPR0600100

;100
#という名目でミルキィのみんなと
#ここで楽しむことになっているんだけど…
That was the pretext for coming
here with the Milky people, but...

;204
…………

;217
OPR0600120

;228
#早く来すぎたかな…。
Maybe I arrived too early...

;259
SRK0600010

;270
#先生～っ！！
Teacher～!!

;289
OPR0600130

;300
#シャーロック…。
Sherlock...

;325
SRK0600020

;336
#はぁ！　はぁ！
#す、すみません！　お待たせしちゃって！
Haa... Haa...
Sorry I took so long!

;416
OPR0600140

;427
#大丈夫だよ…
#僕も今来たばかりだから。
Don't worry,
I just arrived myself.

;483
SRK0600040

;494
#うう…こんな日に遅刻しちゃうなんて…。
Uuu.. To be late on such an important day...

;552
SRK0600050

;563
#昨日のうちに準備もしたし
#忘れないように部屋中メモも張って…。
Even though I made all
the preparations yesterday
and put a memo in my room to not forget...

;655
SRK0600060

;666
#なのに…あたし…。
But still... I...

;694
OPR0600150

;705
#はは、そんなに気にやまなくても…。
Haha, don't worry about it so much...

;757
SRK0600070

;768
#うう…。
Uuu...

;781
OPR0600160

;792
#ほら、天気もいいし。
#今日はめいっぱい楽しんで欲しいんだ。
Look, how good the weather is.
I want you to enjoy yourself
as much as you can today.

;878
SRK0600080

;889
#ダメです！
That's no good!

;905
OPR0600170

;916
#え…？
Eh?

;926
SRK0600090

;937
#先生も楽しんでください！
Teacher should have fun too!

;974
SRK0600100

;985
#先生あってのミルキィホームズです！
#一緒に楽しまなくちゃ意味ありません！
Milky Holmes owes everything to Teacher!
Not having fun all together is meaningless!

;1092
OPR0600180

;1103
#そ、そう…
#なんだかすごい気合いだね…。
O-Okay...
You sound quite energetic...

;1162
SRK0600130

;1173
#はい！　気合入りまくりです！
Yes! Maximum excitement!

;1216
OPR0600190

;1227
#ハハハ…
Hahaha...

;1240
OPR0600200

;1251
#あれ？　ところでみんなは？
Where is everybody, by the way?

;1291
SRK0600140

;1302
#え？　まだ来てないんですか？
Eh? They haven't arrived yet?

;1345
SRK0600150

;1356
#あたしより先に来てると
#思ったんですけど…。
I thought they would come before me...

;1421
OPR0600210

;1432
#ん…？　ネロからだ…。
Hm... Nero's calling...

;1466
OPR0600220

;1477
#もしもし…
Hello...

;1493
NER0600010

;1504
#あ、小林～？
Ah, Kobayashi～?

;1523
OPR0600230

;1534
#今どこにいるんだい？
#僕とシャーロックはもう…
Where are you now?
Sherlock and I are already...

;1602
NER0600020

;1613
#ごっめ～ん…
#ちょっと行けなくなっちゃったんだよね～。
Sorry～...
Something came up～

;1693
OPR0600240

;1704
#え…？　行けないってどういう…
Eh? You aren't coming, you mean?

;1750
NER0600030

;1761
#急用だよ、急用。
#急にはずせない用事ができちゃってさ～。
Urgent business.
Something I absolutely need to do～

;1844
NER0600040

;1855
#あっ、ちなみにエリーとコーデリアもだから。
Same for Eri and Cordelia, by the way.

;1919
OPR0600250

;1930
#はぁ！？
Ha?!

;1943
NER0600050

;1954
#じゃ、そんなわけで。
Well, that being said...

;1985
OPR0600260

;1996
#ま、待ってくれ！
#三人とも突然急用なんていったい何の！？
W-Wait! What's up with all three
of you suddenly having things to do!?

;2079
NER0600060

;2090
#とにかく急用は急用なんだって。
#じゃ～ね～。
Anyhow, urgent business is urgent business.
See you later～

;2155
OPR0600270

;2166
#あ…。
Ah...

;2176
OPR0600290

;2187
#切れた…。
She hang up...

;2203
OPR0600300

;2214
#どういうことだよ…。
#三人とも急用だなんて…。
What is this...
Urgent things to do, all of a sudden...

;2282
SRK0600160

;2293
#あの…ネロ…
#どうかしたんですか？
Um...
Is something wrong with Nero?

;2343
OPR0600310

;2354
#え～と…それが…
Well... apparently...

;2379
OPR0600320

;2390
#今日は来られないって…
She can't come today...

;2424
SRK0600170

;2435
#え！
Eh!

;2442
OPR0600330

;2453
#しかもエルキュールとコーデリアも…。
And it's the same for Hercule and Cordelia...

;2508
SRK0600180

;2519
#ええ！？　そんな！
#どうしてなんですか！？
Ehh!? Why is that?!

;2581
OPR0600340

;2592
#急用って言ってたけど
#キミは何か聞いてないのかい？
Urgent business they said.
Do you know something?

;2666
SRK0600190

;2677
#聞いてないです…何も…
No... They didn't tell anything...

;2711
OPR0600350

;2722
#そうか…
I see...

;2735
SRK0600200

;2746
#みんな…どうして…
Everybody...

;2774
SRK0600210

;2785
#今日はみんなと…先生と…
#一緒に遊べるって…
Even though we decided
to have fun together today...

;2850
SRK0600220

;2861
#そんなの…初めてだったのに…
Even though it's the first time
we do something like this...

;2904
SRK0600230

;2915
#なのに…
And yet...

;2928
SRK0600240

;2939
#う…。
Uu...

;2949
OPR0600360

;2960
#シャーロック…
Sherlock...

;2982
OPR0600370

;2993
#えと…残念だけど…
Um...
That's disappointing, but...

;3021
OPR0600380

;3032
#今日はみんなの都合が悪いみたいだから
#また別の日に…
Everybody is busy today,
maybe another time...

;3109
SRK0600260

;3120
#別の日っていつですか…？
Another time? But when?

;3157
OPR0600390

;3168
#え…？
Eh?

;3178
SRK0600270

;3189
#明日ですか…？　明後日ですか…？
#それともその次の日…？
Tomorrow? Or the day after tomorrow?
Or maybe the day after?

;3272
OPR0600400

;3283
#それは…。
Well...

;3299
SRK0600280

;3310
#あたし素敵な思い出が欲しいんです…
#絶対に忘れないような…。
I want to create beautiful
memories of this day...
Something that I will never forget...

;3399
SRK0600290

;3410
#みんなと…
#先生と一緒にみんなと遊べるんだって…
With everyone...
Even though we decided to have fun together...

;3481
SRK0600300

;3492
#楽しみにしていたのに…
I looked forward to it...

;3526
SRK0600310

;3537
SRK0600320

;3548
#あ…
Ah...

;3555
SRK0600330

;3566
#ご、ごめんなさい…
#あたし、先生を困らせるようなことを…
S-Sorry for annoying you with this...

;3649
SRK0600340

;3660
#そうですよね…
#また、みんなの都合のいい日にすれば…
You are right...
We can always come back another day...

;3737
OPR0600430

;3748
#僕と二人…。
The two of us...

;3767
SRK0600350

;3778
#え？
Eh?

;3785
OPR0600440

;3796
#ダメかな…？　僕と二人だけじゃ…？
Would it be fine?
To go together, the two of us?

;3848
OPR0600450

;3859
#みんなとはまた別の日に来るとして…
We'll come back another
time with everebody else...

;3911
OPR0600460

;3922
#今日は…僕と二人で…。
But today...

;3956
OPR0600470

;3967
#どうかな？
How about it?

;3983
OPR0600480

;3994
#あ、いや…
Ah, I mean...

;4010
OPR0600490

;4021
#僕と二人だけじゃ…
#きっと楽しくないと思うし…
With only me it sure won't be any fun...

;4089
OPR0600500

;4100
#無理にってわけじゃないけど…
I'm not forcing you or anything...

;4143
SRK0600400

;4154
#い…
A...

;4161
SRK0600410

;4172
#いいんですか…？
#あたしと…二人だけでなんて…
Are you sure?
With me alone...

;4240
OPR0600520

;4251
#もちろん…
#キミがよければ…だけど…。
Of course...
If you don't mind, that is...

;4307
SRK0600420

;4318
#もちろんいいに決まってます！
Of course I don't mind!

;4361
SRK0600430

;4372
#で、でも…本当に本当に…
#あたしなんかのために…そんな…
B-But... Really...
Wasting your time on me...

;4455
OPR0600530

;4466
#「なんか」じゃない。
It's not wasted.

;4497
SRK0600440

;4508
!

;4512
OPR0600540

;4523
#僕は約束した…
Didn't I promise?

;4545
OPR0600550

;4556
#今日一日、キミたちと楽しむって…
To enjoy to whole day with everyone...

;4605
OPR0600560

;4616
#「キミたち」には
#もちろんシャーロックも含まれるわけで…
And "everyone" also includes you...

;4699
OPR0600570

;4710
#だから…
That's why...

;4723
OPR0600580

;4734
#このままキミを…帰したくないんだ。
I don't want to send you
back home just like that.

;4786
SRK0600450

;4797
#先生…。
Teacher...

;4810
OPR0600590

;4821
#言ってしまった…。
I've said it...

;4849
OPR0600610

;4860
#正直…あまり自信はないんだけど…
But in all honestly,
I don't have any self-confidence...

;4909
OPR0600620

;4920
#でも…。
But...

;4933
SRK0600460

;4944
#あ、あたし…やっぱり…
As I thought, if it's me...

;4978
SRK0600470

;4989
#今日は遠慮して…。
I should go back and...

;5017
OPR0600630

;5028
#行こう、シャーロック！
Let's go, Sherlock!

;5062
SRK0600480

;5073
#あ…！
Ah...!

;5083
OPR0600640

;5094
#まずは何をする！？
#キミの一番行きたいアトラクションから…
Which attraction do you want to try first?!

;5180
SRK0600490

;5191
#せ、先生…
#そんなに引っ張られると…痛いかも…。
T-Teacher...
It kind of hurts if you pull me like that...

;5262
OPR0600650

;5273
#あ！　ご、ごめん！
Ah! S-Sorry!

;5301
SRK0600500

;5312
#は～、楽しかったです～！
Ha～ That was fun～!

;5349
SRK0600510

;5360
#グルッて回ったらみんなさかさまに見えて
#なんだか別世界に行っちゃったみたいで…！
From upside down it somewhat feels
like I stepped into another world!

;5479
SRK0600520

;5490
#先生はどうでした？
How was it, Teacher?

;5518
OPR0600660

;5529
OPR0600670

;5540
#た…楽しかったよ…もちろん…。
A-Amazing...of course...

;5586
SRK0600530

;5597
#ですよね！？　次は何にしようかな～！
Exactly!
Well, where should we go next～

;5652
OPR0600680

;5663
#まさか…絶叫回転系五連発とは…
So that's why they call it
Quintuple Twisting Rollercoaster...

;5709
OPR0600690

;5720
#正直…キツかった…。
Frankly...
It was pretty harsh...

;5751
SRK0600540

;5762
#あ！　あれにしましょう！
#スペースローリングサンダー！
Ah! Let's do that!
Space Roaring Thunder!

;5842
OPR0600700

;5853
SRK0600550

;5864
#グルグル回ってて、とっても楽しそう～！
Spinning around is so much fun～!

;5922
OPR0600710

;5933
#い、いや…待って…
N-No, wait...

;5961
SRK0600560

;5972
#早く！　早く！
Come on! Come on!

;5994
OPR0600720

;6005
#ええ～…！
Ehh～!

;6021
SRK0600570

;6032
#あ～、こわかった～！
Ah～, scary～!

;6063
OPR0600730

;6074
――――

;6087
SRK0600580

;6098
#おもしろいですよね～！
#いろいろな乗り物があって！
It's so exciting～!
There are plenty of attractions left～!

;6172
OPR0600740

;6183
SRK0600590

;6194
#どんどん行きましょう！　次はアレ！
Let's try them all!
This one's next!

;6246
OPR0600750

;6257
OPR0600760

;6268
#メ、メリー…ゴーランド…
Me-Merry... Goland...

;6305
SRK0600600

;6316
#あ…子どもっぽいの…イヤですか…？
Ah... You don't like childish stuff?

;6368
OPR0600770

;6379
#いや…そういうことじゃなくて…
No... That's not it...

;6425
OPR0600780

;6436
#また…回るんだなと…
A spinning attraction, again...

;6467
OPR0600790

;6478
#う…！
Uu...!

;6488
SRK0600610

;6499
#先生！？
Teacher?!

;6512
SRK0600620

;6523
#どうしたんですか！？
#しっかりしてください、先生！
Are you alright?!
Hang in there, Teacher!

;6597
OPR0600810

;6608
#ダメだ…気が遠くなって…
No good...
I'm going to faint...

;6645
OPR0600800

;6656
#うう…。
Uu...

;6669
OPR0600850

;6680
#ごめん…
Sorry...

;6693
SRK0600660

;6704
#そんな、あたしこそごめんなさい。
No, I'm the one who should say that.

;6753
OPR0600860

;6764
#いや…キミは何も悪くない…
#悪いのは僕だ…。
You haven't done anything wrong...
I'm the one at fault...

;6829
OPR0600870

;6840
#正直に言えばよかったんだよな…
#絶叫系は苦手だって…。
To be honest,
I can't handle rollercoasters very well...

;6920
SRK0600670

;6931
#あたしも、はしゃぎすぎました…
And I got overexcited...

;6977
SRK0600680

;6988
#先生のことを考えずに…
#自分のことしか頭になくて…
Without even considering Teacher...
Only thinking about myself...

;7062
SRK0600690

;7073
#本当にごめんなさい…。
I'm really sorry...

;7107
OPR0600890

;7118
#ダメだな…。
I'm no good...

;7137
SRK0600710

;7148
OPR0600900

;7159
#僕はあいかわらずダメだ…
#本音を言うことができない…
I'm no good, as usual...
I can't never say what's on my mind...

;7236
OPR0600910

;7247
#それを隠す必要のない相手にまで…。
I hide it even from my companions...

;7299
OPR0600920

;7310
#そういうのはよくないって…
#わかってるんだけど…
I know it's a bad habit, but still...

;7381
SRK0600720

;7392
OPR0600930

;7403
#僕は…ダメだ…。
I'm hopeless...

;7428
SRK0600730

;7439
#そ…
That...

;7446
SRK0600740

;7457
#そんなことありません。
That's not true.

;7491
SRK0600750

;7502
#先生は…素敵です…。
Teacher is... fantastic...

;7533
OPR0600940

;7544
SRK0600760

;7555
#初めて会ったときのこと覚えてますか？
Do you remember when we first met?

;7610
OPR0600950

;7621
#会ったときのこと…？
The first time we met...?

;7652
SRK0600770

;7663
#はい…。
Yes...

;7676
SRK0600780

;7687
#あたしのトイズを見て…
#先生が言ってくれた言葉…
When you saw my Toys, you told me...

;7758
SRK0600800

;7769
#かけがえのないあたしの才能…。
That they were my irreplaceable talent...

;7815
SRK0600810

;7826
#あたしだけがおこせる奇跡だって…。
A miracle that only I can bring forth...

;7878
SRK0600820

;7889
#覚えてます？
Do you remember?

;7908
OPR0600960

;7919
#ああ…もちろん…。
Yes...
Of course...

;7947
SRK0600830

;7958
#すごくうれしかったんですよ…。
I was really happy!

;8004
OPR0600970

;8015
#うん…まさか泣くとは思わなかったな…。
Yeah...
You don't remember crying, I guess...

;8073
SRK0600840

;8084
#あれ？　そうでしたっけ？
Ah? There was such a thing?

;8121
OPR0600980

;8132
#おいおい、キミこそちゃんと覚えてるのかい？
Hey, you sure you remember everything?

;8196
SRK0600850

;8207
#えへへ…。
Ehehe...

;8223
SRK0600860

;8234
SRK0600870

;8245
#ミルキィのみんなと過ごした時間…
The time spent with Milky Holmes,

;8294
SRK0600880

;8305
#特に先生が来てからは…
#とっても素敵なことばかり…。
especially, ever since Teacher arrived,
was filled with great moments...

;8382
SRK0600890

;8393
#あたし絶対に忘れません。
Moments that I weill never forget.

;8430
SRK0600900

;8441
#今日のデートのことだって…。
Today's date at the amusement park...

;8484
OPR0600990

;8495
SRK0600920

;8506
#遊園地って不思議ですね。
#なんだかいつもと違うカンジになれます。
It's wonderful.
Somehow, it feels different than usual.

;8601
SRK0600930

;8612
#いつもなら言えないことも…
#言えちゃうみたい…
I'm able to say things
that I couldn't normally say...

;8680
OPR0601010

;8691
#ああ、そうだね…
Ah, indeed...

;8716
SRK0600950

;8727
#先生…
Teacher...

;8737
OPR0601020

;8748
#なんだい？
What is it?

;8764
SRK0600960

;8775
#先生のトイズは…
#どんなタイプだったんですか？
Your Toys...
What kind of Toys were they?

;8843
OPR0601030

;8854
SRK0600970

;8865
#あ、ごめんなさい…。
Ah, sorry...

;8896
SRK0600990

;8907
#調子に乗っちゃった…
#忘れてください、いま言ったこと…。
I got caught up in the moment...
Please forget what I just said...

;8990
OPR0601050

;9001
#いいよ…
It's alright...

;9014
SRK0601000

;9025
OPR0601060

;9036
#今なら話せる気がする…。
I think it's the
right moment to talk about it...

;9073
OPR0601070

;9084
#僕の過去…
In the past...

;9100
OPR0601080

;9111
#僕が持っていた…トイズは…
The Toys I had were...

;9151
ALL0600010

;9162
OPR0601100

;9173
#なんだ！？　今の光！？
What was that light just now?!

;9207
SRK0601020

;9218
#先生！！　あれ！！
Teacher!! Look!!

;9246
SRK0601030

;9257
#火事…？　爆発…？
A fire? An explosion?

;9285
OPR0601110

;9296
#あれは、ヨコハマ火力発電所なんじゃ…。
That's the Yokohama power plant...

;9354
SRK0601040

;9365
#きゃ！
Ah!

;9375
OPR0601120

;9386
#観覧車が止まった！？　停電！？
The Ferris wheel stopped?!
A power outage?!

;9432
OPR0601130

;9443
#やっぱり、発電所が…！
As I thought, the power plant...!

;9477
OPR0601140

;9488
#！！！
!!!

;9498
SRK0601050

;9509
#な、なに、この音…？
W-What is this sound?

;9540
SRK0601060

;9551
#先生…どうしましょう…。
Teacher, what should we do?

;9588
SRK0601070

;9599
#先生…？
Teacher...?

;9612
OPR0601170

;9623
#開幕…
The beginning...

;9633
SRK0601080

;9644
OPR0601180

;9655
#開幕を告げる…鐘の音…
The bells announcing the beginning...

;9689
SRK0601090

;9700
#開幕って…いったい何が…
The beginning... of what...

;9737
OPR0601190

;9748
#忘れない…この音…
#忘れられるはずもない…
This sound...
How can I forget forget it...

;9810
SRK0601100

;9821
#あ、あの…
E-Excuse me...

;9837
OPR0601200

;9848
#でもまさか…そんな…
But that's....
Don't tell me...

;9879
SRK0601110

;9890
#どうしたんですか、先生？
What's going on, Teacher?

;9927
ELU0600010

;9938
#ははははは！
Hahahahaha!

;9957
SRK0601120

;9968
OPR0601210

;9979
#バ、バカな！！！
N-No way!!

;10004
ELU0600020

;10015
#多くの紳士淑女には初の御挨拶となる…
#まずは自己紹介をさせていただこう…
Greetings, ladies and gentlemen...
First, allow me to introduce myself...

;10122
ELU0600030

;10133
#我が名はＬ…
My name is L...

;10152
ELU0600040

;10163
#怪盗Ｌ！！
Phantom L!!

;10179
SRK0601130

;10190
#Ｌ！？
L!?

;10200
ELU0600050

;10211
#さぁ、華麗なる舞台の始まりだ！
And now, let us begin our splendid performance!

;10257
ELU0600060

;10268
#史上最大！　空前絶後のショー！
The greatest show in history!
The first and probably the last!

;10314
ELU0600070

;10325
#ただいまより六時間後…
In 6 hours from now...

;10359
ELU0600080

;10370
#この街は…消える！！！
This district will disappear!!!

;10404
SRK0601140

;10415
#ええっ！？
Ehh!?

;10431
ELU0600090

;10442
#怪盗に不可能はない！
#怪盗は神に等しき奇跡の体現者！
Nothing is impossible for Phantom Thieves!
Phantom Thieves are the
embodiments of God's miracles!

;10519
ELU0600100

;10530
#私の起こす奇跡を…
My miracle...

;10558
ELU0600110

;10569
#諸君たちの目と！　耳と！　そして魂で！
Ladies and gentlemen!
Your eyes, your ears, and your soul...

;10627
ELU0600120

;10638
#あますところなく堪能してくれたまえ！
Enjoy it with everything you have!

;10693
ELU0600130

;10704
#フフフフフフ…
Huhuhu...

;10726
ELU0600140

;10737
#ハァーッハッハッハッハッハッ！！！
Haahahahaha!!!

;10789
SRK0601150

;10800
#せ…先生…？
T-Teacher...?

;10819
SRK0601160

;10830
#怪盗Ｌって…五年前に…先生が…
That Phantom L...
5 years ago, Teacher was...

;10876
OPR0601230

;10887
#ウ…ウソだ…
#そんなこと…ありえない…
N-No way...
This is... impossible...

;10943
OPR0601240

;10954
#怪盗Ｌは…
Because Phantom L...

;10970
OPR0601250

;10981
#怪盗Ｌはあのとき確かに…！
That time, I'm sure he...

;11021
OPR0601260

;11032
SRK0601170

;11043
#あたしのPDA…
My PDA...

;11062
SRK0601180

;11073
#この音は直接通信の…
It's a direct call...

;11104
OPR0601270

;11115
#とにかく出て…！
Pick it up!

;11140
SRK0601190

;11151
#あ、はい！
Ah, yes!

;11167
NER0600070

;11178
#シャロ！
Sharo!

;11191
SRK0601200

;11202
#ネロ！？
Nero!?

;11215
NER0600080

;11226
#今からエリーが動かすからね！
#一応気をつけて！
Eri will now rotate the Ferris wheel!
Be careful!

;11294
SRK0601210

;11305
#え！？　え！？　動かすって！？
#エリーさん！？
Eh?! Eh?! Rotate the wheel?!
Eri will?!

;11373
ALL0600020

;11384
#わ！
Wha!

;11391
SRK0601230

;11402
#う、動いてる！？
#エリーさんが回してるの！？
W-We are moving!
Eri is turning the wheel?!

;11467
NER0600090

;11478
#そうだよ！　ゴンドラのナンバーは！？
Yes! What's your gondola number?!

;11533
SRK0601240

;11544
#え！？　ゴンドラのナンバー！？
Eh?! The gondola number?!

;11590
NER0600100

;11601
#どこかに書いてあるでしょ！？
#何番に乗ってるの！？
It should be written somewhere!
Which gondola are you in?!

;11675
SRK0601250

;11686
#え、えと…！
Um, let me see...

;11705
SRK0601260

;11716
#59番！　59番だよ！
59! Number 59!

;11742
NER0600110

;11753
#わかった！
Got it!

;11769
SRK0601270

;11780
#あ、ちょっとネロ！　ネロ！？
Ah, Nero, wait! Nero?!

;11823
SRK0601280

;11834
SRK0601290

;11845
#先生、あたしなにがなんだか…！
Teacher,
I don't really understand what's going on...

;11891
OPR0601290

;11902
#落ちつくんだ、シャーロック。
Calm down, Sherlock.

;11945
SRK0601300

;11956
#は、はい…。
Y-Yes...

;11975
OPR0601300

;11986
#おまえこそ落ちつけ！　小林オペラ！
You need to calm down too!
Kobayashi Opera!

;12038
OPR0601310

;12049
#さっきの放送…！
The broadcast from before...

;12074
OPR0601320

;12085
#本当なのか…？
#本当にあの怪盗Ｌなのか…？
I wonder if it's true...
Is it really Phantom L?

;12147
OPR0601330

;12158
#六時間後に街は消えると言っていた…。
He said the district
will disappear in 6 hours...

;12213
OPR0601340

;12224
#もしＬの偽物だったとしても…
Even if it isn't the real L...

;12267
OPR0601350

;12278
#その意味は！？
What's the meaning behind this?!

;12300
SRK0601310

;12311
SRK0601320

;12322
#あたし…
I...

;12335
SRK0601330

;12346
#ものすごくよくないことが起きるような…
#そんな気がします…。
I feel like something
really bad will happen...

;12435
OPR0601360

;12446
#大丈夫…
Don't worry...

;12459
OPR0601370

;12470
#大丈夫だよ…。
Everything will be fine...

;12492
CDR0600010

;12503
#シャロ！　大丈夫！？
Sharo! Are you alright?!

;12534
SRK0601340

;12545
#あ、はい。
Ah, yes.

;12561
NER0600120

;12572
#小林は？
And Kobayashi?

;12585
OPR0601380

;12596
#問題ない。
I'm fine.

;12612
SRK0601350

;12623
#ねぇ、ネロ！？
#いったいなにがおきたの！？
Hey, Nero, what happened!?

;12685
SRK0601360

;12696
#どうしてみんながここにいるの！？
Why is everybody here?!

;12745
NER0600130

;12756
#え！？
Eh?!

;12766
NER0600140

;12777
#そ、それは…その…
Well, that's...

;12805
NER0600150

;12816
#尾行してたから…
We were following you...

;12841
SRK0601370

;12852
#尾行！？
Following?!

;12865
CDR0600020

;12876
#い、今はそんなこと
#どうでもいいでしょう！？
W-We have bigger problems right now!

;12941
ERI0600010

;12952
#うん…かなりの非常事態だと思う…。
Yes... we have quite an emergency here...

;13004
CDR0600030

;13015
#そうよ！　そう！
#電話だって通じないんだから！
That's right!
The phones can't even connect!

;13083
SRK0601380

;13094
#電話が？
The phones?

;13107
ERI0600030

;13118
#電波が入っていないの…。
There's no signal...

;13155
OPR0601390

;13166
#停電…そして通信の遮断…
The power and the communications are cut off...

;13203
OPR0601400

;13214
#いけない…このままだと街全体が…
At this rate, the whole town will...

;13263
NER0600160

;13274
#ねぇ小林…
Hey, Kobayashi...

;13290
OPR0601410

;13301
#え？
Eh?

;13308
NER0600170

;13319
#さっきのやつ…
#あれって本物の怪盗Ｌなの？
The guy from before...
Was it the real Phantom L?

;13381
OPR0601420

;13392
CDR0600040

;13403
#ちょっとネロ！　不謹慎でしょう！？
Please Nero!
Pay attention to what you are saying!

;13455
CDR0600050

;13466
#教官は怪盗Ｌのせいで…
#トイズを…その…。
Because of Phantom L...
Professor's Toys are...

;13528
NER0600180

;13539
#だってしかたないじゃない…
Well, that's how it is...

;13579
NER0600190

;13590
#この事態を引き起こした人物が
#怪盗Ｌを名のってる。
The person responsible for
this situation calls himself Phantom L.

;13664
NER0600200

;13675
#今わかってるのはそれだけで
#Ｌに詳しいのは小林だけなんだから。
That's all we got right now.
And only Kobayashi knows L personally.

;13767
CDR0600060

;13778
#そ、それは…そうだけど…。
T-That's... true...

;13818
OPR0601430

;13829
#わからない…
I don't know...

;13848
OPR0601440

;13859
#正直、僕にもわからないんだ。
#あれが本物の怪盗Ｌなのか…。
Honestly, I don't know myself...
Is it the real Phantom L or not...

;13945
OPR0601450

;13956
#でもあの音…
#放送前になったあの鐘の音は…
But that sound...
The sound of bells before the broadcast...

;14018
OPR0601460

;14029
#怪盗Ｌが犯行直前に必ず鳴らしていたものと…
#同じだった…。
The same sound played every time
Phantom L committed a crime...

;14115
NER0600210

;14126
SRK0601400

;14137
#と…
A...

;14144
SRK0601410

;14155
#とにかく…
Anyway...

;14171
SRK0601420

;14182
#学院に行ってみませんか？
Should we go back to the academy?

;14219
CDR0600080

;14230
#学院に？
The academy?

;14243
SRK0601430

;14254
#会長なら…アンリエットさんがいれば
#何かわかるかも！
The chairman, Miss Henriette
might be able to help!

;14331
OPR0601470

;14342
#そうだね！
Good idea!

;14358
OPR0601480

;14369
#あの学院なら
#非常用電源くらいあるだろうし…
There should also be
an emergency power supply...

;14434
OPR0601490

;14445
#IDO直轄の施設だから
#衛星回線があるはず。
And a satellite link as well,
since the academy is part of IDO.

;14504
OPR0601500

;14515
#学院に行ってみよう！
Let's go to the academy!

;14546
ALL0600030

;14557
#はいっ！
Yes!

;14570
@ヨコハマ大停電

