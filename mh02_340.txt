;0
__main

;7
@Lclearscreenall

;24
@エリーのキーヒント

;53
OPR0203810

;64
Hercule.

;86
ERI0200570

;97
Ah... Mr Kobayashi...

;122
OPR0203820

;133
Did you find something?

;167
ERI0200580

;178
No...
There wasn't anything particularly suspicious...

;230
ERI0200590

;241
So I used my PDA to look
into the building's data...

;324
ERI0200600

;335
This is it... What do you think?

;406
OPR0203830

;417
Let's see...

;436
ERI0200630

;447
Mr K-Kobayashi...

;472
OPR0203840

;483
What is it?

;496
ERI0200640

;507
About that item... I didn't find...
anything that might be a problem...

;608
OPR0203850

;619
I see... then...

;650
2002

;655
ERI0200610

;666
That item, right... I'll look into it...

;724
ERI0200650

;735
That item... feels strange...

;784
OPR0203860

;795
Yeah, all the buildings have
the "王" character in their name.

;869
ERI0200660

;880
But the companies running the buildings...
are all different...

;951
OPR0203870

;962
It's tax avoidance.

;975
ERI0200670

;986
Tax avoidance...?

;999
OPR0203880

;1010
Anyway, take a deeper look into it.
For example, check the shareholders...

;1099
ERI0200680

;1110
Alright...

;1135
............

;1148
ERI0200700

;1159
Ah...!

;1169
OPR0203890

;1180
See.

;1193
ERI0200710

;1204
They're all... 100% owned by...

;1230
OPR0203900

;1241
Right, this is an important clue...

;1293
@カードの秘密

