;0
__main

;7
@Lclearscreenall

;24
@ストーンリバーとの決着

;59
CDR0301040

;70
Hah! Hah! Hah!

;113
CDR0301050

;124
P...!

;134
CDR0301060

;145
Pointing a blade... at my... precious friends...

;204
CDR0301070

;215
I won't forgive you!

;243
NER0300890

;254
Did you... get him...?

;282
CDR0301080

;293
Yes...!

;306
OPR0302100

;317
Everyone open your PDAs!

;348
OPR0302110

;359
!

;363
CDR0301090

;374
*sigh*...*sigh*...

;396
OPR0302120

;407
Cordelia!
The phantom thief! Where's Stone River!?

;472
CDR0301100

;483
Eh?

;490
CDR0301110

;501
I just finished dealing with him...

;529
CDR0301120

;540
He's gone!?

;565
STN0300460

;576
What a blunder... how could I...

;628
OPR0302130

;639
Where are you!? Stone River!?

;685
STN0300470

;696
To think the sword had that kind of trap in it...

;754
NER0300900

;765
Come out here!!

;784
STN0300480

;795
I can't do that, but I'll acknowledge my defeat.

;872
SRK0300990

;883
Who cares about that!!
Please turn Kokoro-chan
and everyone back to normal!!

;996
STN0300490

;1007
Don't fret... My power is cancelled by daylight.

;1069
OPR0302140

;1080
OPR0302150

;1091
I see... That's why the windows were boarded up.

;1165
STN0300500

;1176
Indeed, then let us meet again sometime.

;1226
STN0300510

;1237
And that time I won't lose... I swear...

;1283
............

;1296
NER0300910

;1307
Hey! Get back here～!

;1332
CDR0301130

;1343
I'll use my Toy to chase him by his smell!

;1386
OPR0302170

;1397
It's meaningless...
He gracefully admitted
his defeat and disappeared.

;1453
OPR0302180

;1464
Meaning he'd secured a path for his retreat...
We probably won't be able to catch him anyway.

;1577
CDR0301140

;1588
――――

;1601
OPR0302190

;1612
Now then...

;1622
ERI0300550

;1633
Mr Kobayashi!?

;1658
NER0300920

;1669
Wait wait!

;1697
SRK0301000

;1708
You'll get cursed too Teacher!

;1748
OPR0302200

;1759
Will I?

;1775
CDR0301150

;1786
Nothings happening?

;1823
OPR0302210

;1834
Yeah...

;1844
OPR0302220

;1855
When Stone River drew the sword,
something like a gas came out.

;1950
OPR0302230

;1961
Judging by his behaviour after,
it was likely a delirium-inducing drug.

;2059
ERI0300560

;2070
A drug?

;2083
SRK0301010

;2094
It wasn't a curse?

;2131
OPR0302240

;2142
That's just my guess, though.

;2173
OPR0302250

;2184
Like the mansion, the people who made the gas
were trying to make it occult-like.

;2300
OPR0302260

;2311
In order to continue their research in secrecy.

;2357
CDR0301160

;2368
Just what were they making that gas for?

;2423
OPR0302270

;2434
Who knows...

;2444
OPR0302280

;2455
It was when the war was going on.
Nobody knows any more...

;2529
OPR0302290

;2540
Alright, there's still sunlight.

;2571
OPR0302300

;2582
Let's unboard those windows
and release all the frozen people.

;2656
SRK0301020

;2667
Ok then, please do it Eri!

;2719
ERI0300570

;2730
NER0300930

;2741
Go and bash them open.

;2793
ERI0300580

;2804
Ehh!?

;2817
SRK0301030

;2828
Carry Kokoro-chan and everybody up
from underground too please!

;2896
ERI0300590

;2907
Ehhh～!?

;2926
OPR0302310

;2937
Hahaha...

;2953
@一件落着

