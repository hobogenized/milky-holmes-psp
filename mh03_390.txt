;0
__main

;7
@Lclearscreenall

;24
@3サク

;33
OPR0303690

;44
Hey, Saku.

;63
TYM0300120

;74
Hm?

;81
OPR0303700

;92
Are you making any progress?

;123
TYM0300130

;134
Mmm～

;144
............

;157
OPR0303720

;168
Are you researching something?

;193
TYM0300160

;204
That.

;214
OPR0303730

;225
That?

;235
OPR0303740

;246
Ah...

;256
OPR0303750

;267
An armband...
It looks quite old, it's all tattered.

;320
OPR0303760

;331
Umm...

;347
OPR0303770

;358
Ta... ki...

;371
OPR0303780

;382
"Takigawa"!?

;404
TYM0300170

;415
Epidemic Prevention and
Water Purification Department of Kanto Army...
aka "Takigawa Unit".

;492
TYM0300180

;503
Epidemic prevention and water purification...
That was the official stance...

;577
TYM0300190

;588
They were carrying out
development of biological weapons...

;649
TYM0300200

;660
Is what it says...

;676
TYM0300210

;687
Biological weapons, it's like totally unreal～

;748
OPR0303800

;759
Takigawa Unit... I've heard of it...

;805
OPR0303810

;816
Hm...?

;826
【A Takigawa Unit armband was found】
～Is this suspicious?～

;927
TYM0300220

;938
Alright, I'm gonna go...

;981
OPR0303830

;992
Ah, what about this armband?

;1017
TYM0300230

;1028
You can have it.

;1041
OPR0303840

;1052
Eh?

;1059
TYM0300240

;1070
I already researched it,
I don't need it any more～

;1132
TYM0300250

;1143
See ya～

;1162
OPR0303860

;1173
What am I going to do with this...

;1216
@非科学的

