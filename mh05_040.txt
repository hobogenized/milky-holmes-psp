;0
__main

;7
@Lclearscreenall

;24
@協同捜査

;38
TYM0500180

;49
#へぇ～ここがあんたらの事務所ね～。
Eh～ So this is where you work～

;101
ZGT0500250

;112
#お？　この並んでるの捜査資料か？
#きっちり整理してやがんな～…。
Hm? Is this the data you've collected?
Perfectly organised and all～...

;207
HSG0500100

;218
#次子さん、勝手にさわるのはどうかと…。
Tsugiko, try not touching
things without permission...

;276
AKT0500290

;287
#ちょっとあんたたち！
#こんなとこにつれてきてどういうつもり！？
Hey, wait a minute!
Why did you bring us here!?

;379
CDR0500130

;390
#何を言ってるの！？
#おしかけてきたのはあなたたちでしょう！？
What?! Aren't you the ones
who invited themselves here?!

;479
TYM0500190

;490
#まぁまぁ、みんな落ち着いて～…
#ほら、これ見てみ…。
Okay okay, calm down everyone～...
Look at this instead...

;567
AKT0500300

;578
#見るって何を…
Look at what...?

;600
…………

;613
AKT0500320

;624
#あ…！
Ah...!

;634
HSG0500110

;645
#これは！？
Is this...?!

;661
TYM0500200

;672
#捜査に関わるデータ～…
#至宝強奪のね～。
The investigation data～...

;731
ZGT0500260

;742
#おいおい勝手に持ってきちゃったのかよ？
Don't tell me you took it without permission?

;800
HSG0500120

;811
#ダメですよ、咲さん。
That's no good, Saku.

;842
TYM0500210

;853
#ダウンロードしたのは
#捜査から外される前だから問題ないっしょ？
It's okay, I downloaded it
before we got dimissed.

;945
HSG0500130

;956
#それは詭弁です。
#小衣さん、何とか言ってください。
You are playing with words here.
Kokoro, please say something.

;1030
AKT0500330

;1041
#よくやったわ、咲！
Good job, Saku!

;1069
HSG0500140

;1080
#そうですよ、よくやりました。
#さすがは咲さんです。
Well done, indeed.
As expected of Saku.

;1154
HSG0500150

;1165
#って違います！
Except not!

;1187
AKT0500340

;1198
#これも、リーダーである小衣の
#日ごろの指導のおかげね。
This too is all thanks
to my brilliant leadership.

;1278
HSG0500160

;1289
#こ…小衣さん？
Ko...Kokoro?

;1311
AKT0500350

;1322
#とにかくこれで小衣たちも
#事件を調べることができるわ！
Anyway, with this, we too can
participate in the investigation!

;1402
HSG0500170

;1413
#あの…私の話…聞いてます…？
Er... did you hear
what I just said...?

;1456
AKT0500360

;1467
#警視のためにも怪盗帝国のアジトを見つけて
#三つの至宝をとりかえすのよ！
For Chief's sake, we'll find the thieves'
hideout and take back the three treasures!

;1571
HSG0500180

;1582
#ちょっと…。
That's...

;1601
AKT0500370

;1612
#それでは全員捜査開始！
Well then,
let's start the investigation!

;1646
NER0500230

;1657
#はい、待った。
Not so fast.

;1679
AKT0500380

;1690
#何よ！　捜査の邪魔しないでくれる！？
What now!?
Are you trying to hinder us?!

;1745
NER0500240

;1756
#いや邪魔はしないけど
#その捜査ってのはどこでやるの？
Not trying to hinder anything but,
where exactly are you going to do it?

;1833
AKT0500390

;1844
#ここに決まってるでしょう！？
#署でやったらバレちゃうじゃない！？
Here of course! We'll be found out
if we go to the police station!

;1939
NER0500250

;1950
#ここ、「僕たちの」事務所なんだよね～。
This is ``our'' office, right～?

;2008
AKT0500400

;2019
#う…！
Uh...

;2029
TYM0500220

;2040
#何？　ダメなの～？
Can't we stay here～?

;2068
NER0500260

;2079
#別にダメじゃないよ…ね、小林？
We don't really mind...
Right, Kobayashi?

;2125
OPR0500510

;2136
#え？　ああ…。
Eh? Yeah...

;2158
NER0500270

;2169
#でもタダってわけにはいかないかなぁ～。
But I wonder how much it will cost you～

;2227
NER0500280

;2238
#ここを使わせてあげるんだから～…
#当然それなりの見返りはほしいよね～？
We'll let you use this place～...
But it's also natural for us to
ask you something in exchange, right～?

;2342
AKT0500410

;2353
#見返りって…まさか…
Something, you say...
Don't tell me you want...

;2384
NER0500290

;2395
#そ～そ～。
That's right～

;2411
AKT0500420

;2422
#この…
You...

;2432
AKT0500430

;2443
#小衣の美しいカラダがほしいのねっ！？
Are you asking for Kokoro's beautiful body?!

;2498
ALL0500050

;2509
#ドテ！
No!

;2519
TYM0500240

;2530
#おいおい…。
Hey hey...

;2549
ZGT0500280

;2560
#まったく…何のドラマの影響だぁ？
Honestly... you watch too much drama.

;2609
AKT0500440

;2620
#わかったわ…愛する警視のためなら…。
I understand... If it's for
the sake of my love for Chief, then...

;2675
NER0500310

;2686
#なわけないでしょ…。
Get real...

;2717
NER0500320

;2728
#情報だよ情報。
Intelligence.
I'm talking about information!

;2750
NER0500330

;2761
#こっちは場所を提供する。
#そっちは事件のデータを提供する。
We let you stay here
and you provide us with information.

;2847
NER0500340

;2858
#悪い取り引きじゃないと思うんだけど？
It's a pretty good deal I think.

;2913
AKT0500450

;2924
#イ、イヤよ！
#誰があんたらなんかに…
N-no way!
Who would want to work with you anyway...

;2977
ZGT0500290

;2988
#わかった、それでいこう。
Understood, let's do this.

;3025
AKT0500460

;3036
#次子！？
Tsugiko!

;3049
ZGT0500300

;3060
#受けた恩はきっちり返す。
#じゃないとお天道様に笑われるよ。 <-----
We'll definitely return you the favour.

;3146
ZGT0500310

;3157
#それにあたしらのPDAだけじゃ効率が悪い…
#だろ咲？
Besides, our PDAs are useless now,
right Saku?

;3228
TYM0500250

;3239
#だね～ここのメインフレームに
#リンクさせてもらった方がいいかも～。
Yep～ Maybe I can connect
to the mainframe here～

;3337
ZGT0500320

;3348
#ほらな？
See?

;3361
AKT0500470

;3372
#な、なによ…リーダーは小衣なのに…。
B-but...
I'm the leader here...

;3427
ZGT0500330

;3438
#たまにはお姉さんの言うことも聞けって。
Sometimes you should listen
to what big sis' says.

;3496
AKT0500480

;3507
#むぅ～…。
Uu～

;3523
TYM0500260

;3534
#それじゃここのアクセスコード～
#教えてくれる～？
Well then～
What's the access code, please～

;3605
NER0500350

;3616
#いいよね、小林？
Its fine, right Kobayashi?

;3641
OPR0500520

;3652
#ああ、OKだ。
Yeah, it's okay.

;3670
SRK0500340

;3681
#ココロちゃん！
#一緒に手がかりを見つけようね！
Kokoro-chan!
Let's search for clues together!

;3749
AKT0500490

;3760
#フン！　小衣が先に見つけるんだから！
Hmph! I'll get them first anyway!

;3815
SRK0500350

;3826
#先生！
#あたし絶対に宝を取り返してみせます！
Teacher!
I'll definitely get the treasure!

;3891
SRK0500360

;3902
#だって、みんなで守った…
Because...

;3939
SRK0500370

;3950
#みんなの想いのこもった宝なんですから！
This treasure is filled
with everyone's feelings!

;4008
CDR0500150

;4019
#そうよね、シャロの言う通りだわ。
Indeed, it is as Sharo says.

;4068
ERI0500150

;4079
#はい…。
Yes...

;4092
NER0500380

;4103
#絶対にできるって、僕たちなら。
Is somebody could do it without fail,
that definitely would be us.

;4149
OPR0500530

;4160
#ああ、がんばろう、みんな。
Yeah, let's do our best.

;4200
@５章マップ用設定

