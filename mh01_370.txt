;0
__main

;7
@Lclearscreenall

;24
@010402_コーデリアのボーナス

;63
CDR0102240

;74
Ahh...!

;87
OPR0105490

;98
Hm?

;105
CDR0102250

;116
Geeze...!

;129
OPR0105500

;140
Is she upset about something?

;171
CDR0102260

;182
Ah... Mr Kobayashi...

;207
Are you feeling alright?

;238
What's wrong?

;263
Do you have to go to the bathroom?

;294
OPR0105510

;305
Are you feeling alright?

;339
OPR0105520

;350
Are you alright?

;363
CDR0102270

;374
Ah...

;381
CDR0102280

;392
Thank you, I'm fine.

;447
OPR0105530

;458
What's wrong? You look unsettled.

;532
CDR0102290

;543
Ah! S-Sorry!

;577
OPR0105540

;588
Do you have to...

;610
OPR0105550

;621
use the bathroom?

;634
CDR0102300

;645
I don't!!!

;667
OPR0105560

;678
Then, what's got you so...

;724
CDR0102310

;735
The truth is...

;745
CDR0102320

;756
I'm worried about every one.

;793
OPR0105570

;804
Everyone... as in, the girls here?

;841
CDR0102330

;852
Yes, about those three.

;886
CDR0102340

;897
If there really is an intruder,
then something bad could happen to them.

;989
CDR0102350

;1000
I'm worried about that.

;1043
............

;1056
OPR0105590

;1067
You're a nice person.

;1095
CDR0102370

;1106
Eh?

;1113
OPR0105600

;1124
Even though the situation is the same for you,
the most important thing on your mind is the girls.

;1210
OPR0105610

;1221
I think that's wonderful.

;1261
CDR0102380

;1272
――――！

;1288
OPR0105620

;1299
With you thinking of them so much...
I won't have to worry, no matter what happens.

;1385
OPR0105630

;1396
Not to mention that your
self defence skills are amazing...

;1448
CDR0102390

;1459
Ah! Um um! I-I-I-I'm! Going to go check o-out!
Some other places too ok! Ok!

;1563
CDR0102400

;1574
B-B-B-Bye!

;1596
OPR0105650

;1607
Ha ha...

;1620
OPR0105660

;1631
She's surprisingly shy.

;1668
@謎の怪盗

