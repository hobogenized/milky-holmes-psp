;0
__main

;7
@Lclearscreenall

;24
@G4

;28
CST0800010

;39
The frequent phantom thieve cases in the
nation that have become a serious problem in
the present day...

;131
CST0800020

;142
Especially in Yokohama where reports of
damage have increased...

;204
CST0800030

;215
The Kanagawa Prefecture Police also reported
that they were in talks about
countermeasures.

;295
NER0800010

;306
Hmm...an increase in phantom thief cases eh...

;349
ERI0800010

;360
Why are there a lot here...in Yokohama...?

;415
SRK0800010

;426
Is it maybe because of that Phantom Thief
Empire that the president showed us before?

;503
CDR0800010

;514
It's thinkable... What do you think, Professor?

;573
OPR0800010

;584
Huh? What are you guys talking about?

;606
CDR0800020

;617
You weren't listening? The news just now...

;685
OPR0800020

;696
Sorry... I was looking for something...

;754
SRK0800020

;765
Looking for something?

;790
NER0800020

;801
That's a pretty big bag.

;841
OPR0800030

;852
Yeah... It should be in here...

;892
OPR0800040

;903
Ah, here it is...

;937
OPR0800050

;948
――――

;961
ERI0800020

;972
That's... a detective badge...

;1000
OPR0800060

;1011
Yeah...

;1024
OPR0800080

;1035
I lost my Toy... and my confidence as a
detective disappeared...

;1106
OPR0800090

;1117
I didn't think I'd ever use it again... I never
thought the day would come that I'd take it up
like this...

;1239
SRK0800030

;1250
Ah, coming!

;1269
OPR0800110

;1280
I wonder who it is?

;1296
TCH0800010

;1307
Good afternoon, everyone.

;1353
SRK0800040

;1364
Ah, Mr Tachi...

;1386
OPR0800120

;1397
What is it?

;1419
TCH0800020

;1430
Miss Henriette is calling for you.

;1473
TCH0800030

;1484
I'm sorry, but please come to the student
council president's room.

;1564
OPR0800130

;1575
Everyone?

;1594
TCH0800040

;1605
Yes...

;1618
OPR0800140

;1629
Alright. Let's go, everyone.

;1676
SRK0800050

;1687
I wonder what it is...?

;1706
NER0800030

;1717
Are we in trouble?

;1733
ERI0800030

;1744
Ehh...!

;1757
CDR0800030

;1768
Like I said, do you have anything to feel guilty
about?

;1820
ANR0800010

;1831
Come in.

;1844
OPR0800150

;1855
!!

;1862
OPR0800160

;1873
K-Kamitsu..?

;1892
KMT0800020

;1903
It's been a long time, Kobayashi.

;1934
SRK0800060

;1945
Is it someone you know?

;1973
OPR0800170

;1984
Yeah...He was a classmate when I was a
student... But, why are you here...?

;2076
ANR0800020

;2087
They seem to want to introduce themselves.

;2115
OPR0800180

;2126
Introduce?

;2136
ANR0800030

;2147
These people are the police's anti-phantom
thief unit...

;2193
ANR0800040

;2204
"GENIUS 4".

;2235
KMT0800030

;2246
Please call us G4.

;2282
AKT0800010

;2293
――――

;2306
TYM0800010

;2317
HSG0800010

;2328
ZGT0800010

;2339
OPR0800190

;2350
Police's anti-phantom thief unit...? Kamitsu,
you became a police officer?

;2430
KMT0800040

;2441
That's not important is it?

;2487
KMT0800050

;2498
Like she just said We're the Yokohama City
Police's anti-phantom thief unit, G4.

;2577
KMT0800060

;2588
The team organised to put an end to the
steadily increasing phantom thief cases.

;2689
KMT0800070

;2700
Currently in the test phase though...

;2728
KMT0800080

;2739
Introduce yourselves in order, starting with
Kokoro.

;2776
AKT0800020

;2787
Yes.

;2797
SRK0800070

;2808
You're called Kokoro-chan!? What a cute
name!

;2888
AKT0800030

;2899
Don't call me Kokoro-chan! You're overly
familiar, aren't you!

;2964
SRK0800080

;2975
Au...

;2988
AKT0800040

;2999
Kokoro is Kokoro, Akechi Kokoro. A beautiful,
brilliant minded, super intelligent, genius girl.

;3079
AKT0800050

;3090
And G4's leader. Think as though you guys
won't be needed any more

;3181
KMT0800090

;3192
Saku.

;3199
TYM0800020

;3210
Okay~, I'm Tohyama Saku.

;3244
TYM0800030

;3255
I'm in charge of like, intelligence gathering~?
Nice to meet you~

;3311
KMT0800100

;3322
Hirano.

;3332
HSG0800020

;3343
Yes, I'm Hasegawa Hirano.

;3386
HSG0800030

;3397
I practice Judo, Kendo, Aikido, amongst
others. I'm mainly in charge of close combat
fighting at crime scenes.

;3501
KMT0800110

;3512
Tsugiko.

;3522
ZGT0800020

;3533
Roger, I'm Zenigata Tsugiko.

;3585
ZGT0800030

;3596
From cars of course, to anything from
helicopters or air planes. Basically, I'm in
charge of flying and driving.

;3730
KMT0800120

;3741
And I fill the role of guiding and supervising
them... Superintendent Kamitsu Rei.

;3821
OPR0800200

;3832
Superintendent!?

;3854
OPR0800210

;3865
When did you...

;3896
KMT0800130

;3907
I believe I said it wasn't important?

;3953
KMT0800140

;3964
In any case, know that the thinking of
"Detectives to Phantom Thieves" doesn't
stand any more.

;4056
KMT0800150

;4067
We will be actively participating in phantom
thief cases. We came today to inform you of
this.

;4156
OPR0800220

;4167
Kamitsu...

;4180
KMT0800165

;4191
Well then, Henriette Mystere. We shall excuse
ourselves.

;4268
ANR0800050

;4279
Yes, thank you very much for taking the
trouble of coming here.

;4350
KMT0800170

;4361
Let's go.

;4374
ALL0800010

;4385
.........

;4398
ANR0800060

;4409
And, that seems to be how it is.

;4452
ANR0800070

;4463
The organising of phantom thieves by
phantom thief Arsene... In short the formation
of the Phantom Thief Empire.

;4549
ANR0800080

;4560
And the IDO's response to that, the
organising of detectives... In short the
formation of you guys, Milky Holmes.

;4673
ANR0800090

;4684
The police wouldn't go the way of watching
with their thumbs in their mouths.

;4776
OPR0800240

;4787
The police's anti-phantom thief unit...

;4830
SRK0800090

;4841
But everyone was nice weren't they.

;4887
NER0800040

;4898
I don't think everyone was.

;4932
ERI0800040

;4943
Miss Akechi was... scary...

;4977
CDR0800040

;4988
Don't you mean rude? That...

;5040
OPR0800260

;5051
Kamitsu...Why are you...?

;5085
ANR0800100

;5096
Well then...

;5106
ANR0800110

;5117
Come here everyone. I have one more thing
to tell...

;5179
ANR0800120

;5190
No, I have something to give you.

;5239
SRK0800100

;5250
What is this...?

;5281
ANR0800130

;5292
Personal digital assistant. A PDA.

;5372
ANR0800140

;5383
Of course it can function as a cell-phone...

;5432
ANR0800150

;5443
Even if a problem occurs at the base station
direct communication is still possible.

;5517
ANR0800160

;5528
Please let it be of use to you.

;5574
SRK0800110

;5585
Waa! It's ok to have it!?

;5634
NER0800050

;5645
Work supplies?

;5670
ERI0800050

;5681
Cute...

;5700
CDR0800050

;5711
It's like a compact.

;5739
Who will take the PDA first?

;5785
Sherlock

;5804
Nero

;5811
Hercule

;5830
Cordelia

;5846
Who will take the PDA next?

;5889
Who will take the PDA third?

;5938
ANR0800170

;5949
Uhuhu, here, for you too, Mr Kobayashi.

;5992
OPR0800270

;6003
Ah, thanks...

;6025
ANR0800180

;6036
Because I will be having everyone working
hard at crime scenes...

;6122
ANR0800190

;6133
You can leave this kind of support to me.

;6188
ANR0800200

;6199
And please don't forget either... That the
detectives' prestige is hanging on this...

;6282
ALL0800020

;6293
SRK0800130

;6304
Yay!

;6317
NER0800070

;6328
I had just been thinking of changing my
cell-phone too~.

;6392
ERI0800070

;6403
It seems very...useful...

;6434
CDR0800070

;6445
The colours were matched to our preferences
too.

;6500
OPR0800290

;6511
They haven't noticed...

;6554
OPR0800280

;6565
That the pressure in her words just now are
saying "Don't lose to the police"...

;6660
11004

;6666
1

;6668
@ラット登場

;6685
@ストーンリバー登場

;6714
@トゥエンティ登場

