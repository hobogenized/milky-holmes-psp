;0
__main

;7
@Lclearscreenall

;24
@フォルトニウムの光

;53
ERI0500500

;64
#はぁ！　はぁ！　はぁ！　はぁ！
Haa... Haa... Haa...

;110
NER0500770

;121
#きっつー！
That was close!

;137
OPR0502050

;148
#ボートを！　残っているボートを探すんだ！
Find... Find a lifeboat!

;209
SRK0500810

;220
#ヘンです！
That's strange!

;236
OPR0502060

;247
#え！？
Eh?!

;257
SRK0500820

;268
#静かすぎませんか！？
Isn't it way too calm?!

;299
CDR0500620

;310
#確かにヘンだわ！
#人の気配がまったくしない！
That sure is strange!
There is no sign of human presence!

;375
SRK0500830

;386
#救命ボートも沢山あります！
There are also plenty of lifeboats left!

;426
OPR0502070

;437
#どうして！？
Why?!

;456
ALL0500150

;467
#きゃ！！
Ah!!

;480
OPR0502080

;491
#と、とにかくボートに乗る…
A-Anyway, let's get on a boat...

;531
AKT0500720

;542
#小林ぃー！！！！！
Kobayashiー!!!!!

;570
SRK0500850

;581
#コ、ココロちゃん！？
Ko... Kokoro-chan!?

;612
TYM0500470

;623
#お迎え登場～！
Friendly faces～!

;645
HSG0500310

;656
#皆さん！！！乗ってください！！！
Get on, everyone!!!

;705
AKT0500740

;716
#ほら！！！　つかまって！！！
Hey!!! Grab this!!!

;759
SRK0500860

;770
#わ！！！
Wa!!!

;783
NER0500800

;794
#イテッ！！！
Ouch!!!

;813
CDR0500640

;824
#きゃ！！！
Ah!!!

;840
HSG0500320

;851
#エルキュールさん！！！　急いで！！！
Hercule!!! Hurry!!!

;906
ERI0500520

;917
#はは、はい…！！
Y-Yes...!!

;942
AKT0500750

;953
#全員乗った！！！？？？
Everybody here???!!!

;987
OPR0502120

;998
#ああ！！！
Aah!!!

;1014
AKT0500760

;1025
#次子、出して！
Take us out, Tsugiko!

;1047
ZGT0500450

;1058
#合点！
Understood!

;1068
AKT0500770

;1079
#もっと離れて！！　早く！！
We need to get farther!! Come on!!

;1119
ZGT0500460

;1130
#わかってるって！
I get it, I get it!

;1155
TYM0500480

;1166
#ふぅ～…。
Haa～...

;1182
OPR0502130

;1193
#おいおい、いったい何が？
What the heck happened?

;1230
HSG0500330

;1241
#ええと…何から説明したものでしょうか…。
Well... Where should I start...

;1302
AKT0500780

;1313
#ボートがおろせなかったのよ。
We couldn't take down the lifeboats.

;1356
HSG0500340

;1367
#ああ、そうです！
Ah, yes!

;1392
HSG0500350

;1403
#来場者を救命ボートに乗せようとしたところ
#ウィンチが動きませんでした。
We put the passengers in the lifeboats but
we couldn't operate the crane to set them afloat.

;1507
HSG0500360

;1518
#それで小衣さんが電源を調べに行ったら
#ものすごい形相でもどってきて…
Then, Kokoro went to check the power generator
and she came back with a terrible expression...

;1619
HSG0500370

;1630
#そのまま私を海に蹴落とし…
#すぐヘリを呼んでこいと…。
During that time, I fell into the water...
And then I tried to call the helicopter...

;1710
OPR0502140

;1721
#ん？　よくわからないな…？
Hm? I don't really get it...

;1761
AKT0500790

;1772
#爆弾よ！！
The bomb!!

;1788
AKT0500800

;1799
#船の中で爆弾を見つけたの！！
I found a bomb on the boat!!

;1842
AKT0500810

;1853
#しかもあの構造…多分…
Also, the composition was probably...

;1887
TYM0500490

;1898
#ねぇ、時間だよ…。
Hey, it's time...

;1926
AKT0500820

;1937
#！！！
!!!

;1947
AKT0500830

;1958
#目を閉じてぇ！！！！！
Close your eyes!!!!!

;1992
…………

;2005
OPR0502150

;2016
#ほ…
Ho...

;2023
OPR0502160

;2034
#他の乗客は…？
And the other passengers?

;2056
HSG0500380

;2067
#大丈夫です…
#県警のヘリを総動員して退避させました…。
They'll be fine...
We evacuated them by helicopter...

;2147
TYM0500500

;2158
#なに考えてんの…怪盗帝国のやつら…？
What were they planning...?
Those Phantom Empire guys...

;2213
AKT0500840

;2224
#あ…悪夢だわ…。
A... A nightmare...

;2249
MovieChapterEnd

;2265
15004

;2271
1

;2273
@６話冒頭

