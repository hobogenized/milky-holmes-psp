# Tantei Opera Milky Holmes

Latest patch available **[here](https://bitbucket.org/truneet/milky-holmes-psp/downloads/mhpatch-0.4.7z)** (v0.4)

### Installation

1. Open DeltaPatcherLite.exe
2. Enter the location of the original ISO file in "Original file".
3. Enter the location of the xdelta patch file in "Xdelta patch".
4. Check "Keep original file" if you don't want to overwrite your original ISO.
5. Click "Apply patch" and wait.

### Contribution
Contributions and bug reports are always welcome, either through the [issue tracker](https://bitbucket.org/truneet/milky-holmes-psp/issues) or by sending a pull request.

### Changelog
0.4 - https://bitbucket.org/truneet/milky-holmes-psp/issue/8/  
0.3 - Fixed a freeze bug in Cosmo World/Sherlock.  
0.2 - Fixed a freeze bug at the end of the Adam's Tear arc.  
0.1 - First release.

### Credits

Sakura!RAILgUnr8s  
desuwa  
KiteSeekers (videos)  
Bitbucket contributors
