;0
__main

;7
@Lclearscreenall

;24
@010102_シャロのボーナス

;57
SRK0102200

;68
Um... Mr Kobayashi...?

;96
OPR0104340

;107
Hm? What is up?

;132
SRK0102210

;143
I'm still worried about the president after all.
Wouldn't it be better
if some people went looking for her?

;244
It's alright.

;254
You're not seeing the whole picture.

;291
There's no point.

;304
OPR0104350

;315
It's alright.

;328
OPR0104360

;339
It's better to not worry too much about it.

;379
SRK0102220

;390
――――

;403
SRK0102230

;414
You... You're right...

;445
OPR0104370

;456
You're not seeing the whole picture...

;496
OPR0104380

;507
I guess...?

;520
SRK0102240

;531
Is that so...

;559
OPR0104390

;570
There's no point.

;586
OPR0104400

;597
Look at this situation.
Where should we start searching for her?

;655
SRK0102250

;666
T-That's right too... isn't it...

;706
OPR0104410

;717
Calm down a bit...

;748
OPR0104420

;759
We still aren't sure about what happened here.

;827
OPR0104430

;838
There is also a possibility
that Miss Henriette was unharmed.

;924
SRK0102260

;935
But...

;948
OPR0104440

;959
Yeah... I can understand how you'd feel uneasy,
given the situation.

;1030
OPR0104450

;1041
However, it's best not to narrow our field of view.

;1096
OPR0104460

;1107
If we look at it from a biased view,
we'll come out with a biased conclusion.

;1187
OPR0104470

;1198
If we want to find the path
leading to the truth, we need to look
at things from multiple perspectives.

;1278
OPR0104480

;1289
So right now, we should be
focusing on the investigation together.

;1350
............

;1363
OPR0104490

;1374
That's what I think, at least...

;1414
SRK0102280

;1425
It's the same...

;1438
OPR0104500

;1449
Eh?

;1456
SRK0102290

;1467
"Look at the problem from multiple sides"

;1504
SRK0102300

;1515
We learned the same thing in class.

;1558
OPR0104510

;1569
Yeah, that's right.

;1600
SRK0102310

;1611
Thank you!

;1645
OPR0104520

;1656
Woah!

;1669
SRK0102320

;1680
If you hadn't told me,
I might've gone on forgetting!

;1769
SRK0102330

;1780
Even though it's such an important thing!

;1820
OPR0104530

;1831
Y-Yeah...

;1850
SRK0102340

;1861
I'm going to examine some more places!

;1928
OPR0104540

;1939
G-Go ahead, I guess...

;1982
SRK0102350

;1993
Yes! Excuse me then!

;2015
OPR0104560

;2026
What a lively kid...

;2048
@謎の怪盗

