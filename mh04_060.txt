;0
__main

;7
@Lclearscreenall

;24
@予告の時間

;41
SRK0400350

;52
It's just about time.

;86
OPR0400540

;97
Yeah, it's almost the time that was
in the phantom thief's crime notice.

;155
NER0400160

;166
We'll protect it though.

;218
CDR0400270

;229
Don't let your guard down, Nero.

;269
ERI0400120

;280
――――

;293
KMT0400150

;304
Kobayashi.

;314
OPR0400550

;325
Kamitsu...

;338
OPR0400560

;349
We can't enter the special display room.

;398
OPR0400570

;409
So we'll standby somewhere else.
There isn't a problem with that is there?

;498
KMT0400160

;509
Hm... I don't think there will be any point...

;540
KMT0400170

;551
In any case, we'll decide
the positioning of the security.

;625
KMT0400180

;636
I'll guard the special display
room that houses the Adam's Tear.

;704
KMT0400190

;715
G4 will handle the public display room.

;739
KMT0400200

;750
And you guys cover the elevator hall.

;805
OPR0400580

;816
So basically we'll protect it in 3 groups?

;856
KMT0400210

;867
Exactly.

;883
OPR0201530

;894
Hm...?

;904
【The 3 groups are: MH, G4 and Kamitsu】
～Is this suspicious?～

;1004
KMT0400220

;1015
Now then, we'll head to our positions...

;1064
ALL0400050

;1075
Ok!

;1085
TYM0400030

;1096
See ya～...

;1118
............

;1131
OPR0400620

;1142
Phantom thief Twenty,
what are you going to do...

;1188
4002

;1194
@４話暴露ショー

