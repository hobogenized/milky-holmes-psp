;0
__main

;7
@Lclearscreenall

;24
@暴露ショー

;41
AKT0300190

;52
Hey! What do you want, gathering us together!?

;114
TYM0300040

;125
Did you solve all the puzzles～
Or something like that?

;196
OPR0301280

;207
Probably... I think I've gotten the gist of it.

;271
HSG0300080

;282
Really!?

;304
ZGT0300040

;315
That's really something.

;346
KMT0300160

;357
Then let us hear it.

;397
OPR0301290

;408
Alright... Then I'll go over things in order.

;473
OPR0301300

;484
First off, about this mansion...

;536
OPR0301310

;547
Kokoro, you found "charms" hung up
here and there right?

;639
AKT0300200

;650
D-Don't make me remember!

;687
OPR0301320

;698
Black writing on yellow paper...
That's called a "hajafu"...

;775
OPR0301330

;786
It's used to seal evil spirits and demons.

;860
OPR0301340

;871
Saki, you found an armband
from the Takigawa unit right?

;935
TYM0300050

;946
Eh～ pretty much～

;971
OPR0301350

;982
You'd found out there was a rumour that
they'd been developing a biological weapon...

;1104
OPR0301360

;1115
But there is one other thing...

;1140
OPR0301370

;1151
It's said there was someone in the unit
researching in supernatural phenomena too...

;1252
OPR0301380

;1263
Hirano, you found earth, water, fire, and wind
written on the 1F posts, right?

;1352
HSG0300090

;1363
Yes.

;1373
OPR0301390

;1384
And those posts were set
in the east, west, south, and north.

;1436
OPR0301400

;1447
4 elements in the 4 directions
means to form a barrier to seal evil spirits.

;1533
OPR0301410

;1544
Tsugiko, you found an old bullet right?

;1599
ZGT0300050

;1610
Oh, the one that was in the drawer? 

;1656
OPR0301420

;1667
Yes, the head was quite black right?
That was most likely silver sulfide.

;1759
OPR0301430

;1770
In other words,
I think that it was a silver bullet.

;1813
OPR0301440

;1824
It's known to defeat werewolves,
but in the west it's said to work
on demons as well.

;1955
OPR0301450

;1966
Meaning...

;1979
OPR0301460

;1990
There exists some kind of evil spirit here...

;2048
OPR0301470

;2059
And there are signs of attempts
to seal or even exterminate it... 

;2151
AKT0300210

;2162
What's that! It makes no sense!
That's not scientific at all!

;2245
OPR0301480

;2256
Right, it's unscientific.

;2290
AKT0300220

;2301
Huh?

;2311
OPR0301490

;2322
I haven't finished yet...

;2362
OPR0301500

;2373
The truth behind this confusion...

;2398
OPR0301510

;2409
The components that will
lead my conjecture to certainty!

;2455
OPR0301520

;2466
Listen to these important clues
the girls have found!

;2540
OPR0301530

;2551
Sherlock!

;2573
SRK0300780

;2584
Yes!

;2594
SRK0300790

;2605
There was a bottle of mineral
water in a mannequin's bag.

;2703
SRK0300800

;2714
But the cap on the bottle had been opened
and it was only half full.

;2815
SRK0300810

;2826
In other words the mineral water
had been partially drank.

;2890
SRK0300820

;2901
Would you put something like
that in a mannequin's bag!?

;2984
SRK0300830

;2995
This is a point we can't ignore!

;3044
OPR0301540

;3055
Alright!

;3065
OPR0301550

;3076
Nero!

;3086
NER0300720

;3097
'Kay～

;3110
NER0300730

;3121
The mannequin I checked out
had underwear put on under the blouse.

;3231
NER0300740

;3242
Would you normally do that?

;3279
NER0300750

;3290
It's a mannequin, so wouldn't you
just put clothes straight on without underwear?

;3379
NER0300760

;3390
I think this is a point we can't ignore.

;3448
OPR0301560

;3459
OPR0301570

;3470
Hercule!

;3492
ERI0300420

;3503
Y-Yes...!

;3522
ERI0300430

;3533
I examined a mannequin...
that had one high-heel taken off...

;3619
ERI0300440

;3630
And, that high-heel...
the bottom of it was worn out...

;3728
ERI0300450

;3739
Mannequins don't move...
Is it even possible for the sole
of the shoe to be worn out...?

;3837
ERI0300460

;3848
I think that this is a point we can't ignore...

;3912
OPR0301580

;3923
OPR0301590

;3934
Cordelia!

;3953
CDR0300890

;3964
Yes.

;3974
CDR0300900

;3985
I found a mannequin with earrings.

;4046
CDR0300910

;4057
But, those earrings were clear studs.

;4128
CDR0300920

;4139
Clear studs are temporary earrings
used when your ears get pierced.

;4222
CDR0300930

;4233
Would you put something like that on a mannequin?

;4294
CDR0300940

;4305
I think that this is a point we can't ignore.

;4366
OPR0301600

;4377
OPR0301610

;4388
This is what I think.

;4413
OPR0301620

;4424
Like the rumours say,
there's something hidden here.
Though it may not be treasure.

;4549
OPR0301630

;4560
In order to make that not known,
the mansion was set up occult-like.

;4682
OPR0301640

;4693
Using the creepy setup to ward people off.

;4758
KMT0300170

;4769
Are you saying the mannequins
on the 2nd floor are part of that?

;4827
OPR0301650

;4838
No.

;4848
OPR0301660

;4859
The set up of the mansion and
the mannequins on the 2nd floor are unrelated.

;4920
OPR0301670

;4931
Kamitsu, you've noticed it too haven't you?
That those aren't mannequins.

;5044
............

;5057
OPR0301680

;5068
The making is too elaborate.
Almost as though they were alive.

;5139
OPR0301690

;5150
In other words those are real people.

;5196
ALL0300130

;5207
Ehh!?

;5220
OPR0301700

;5231
To make a person look just like a mannequin.
The only thing that can do that is...

;5332
KMT0300190

;5343
A Toy...

;5359
OPR0301710

;5370
Right!

;5380
OPR0301720

;5391
Meaning this whole thing is a phantom thief case!

;5440
OPR0301730

;5451
The people that came here for the hidden item
were made mannequins by the thief's Toy!

;5570
OPR0301740

;5581
This is the truth behind the missing persons,
and the ghost that appears in the mansion!

;5673
AKT0300240

;5684
No way... this... was a phantom thief case...

;5733
KMT0300200

;5744
The yellow eyed ghost...
Its true identity was a phantom thief...

;5815
OPR0301750

;5826
Yeah, most likely.

;5848
KMT0300210

;5859
So then what is the hidden thing
in this mansion you talk about?

;5924
OPR0301760

;5935
I don't know...

;5963
OPR0301770

;5974
But I have some idea of where
the location it's hidden may be.

;6057
KMT0300220

;6068
Where?

;6087
OPR0301780

;6098
Follow me.

;6123
OPR0301790

;6134
There was something I noticed when
I first checked out the 1st floor.

;6205
OPR0301800

;6216
This post is fake.

;6247
SRK0300840

;6258
Eh!?

;6268
NER0300770

;6279
Ah, you're right...

;6301
CDR0300950

;6312
Looking closely,
there's a small gap within the wall.
There's just a thin board put there.

;6410
ERI0300470

;6421
Why... did they do this...?

;6461
OPR0301810

;6472
To hide the position of the real post.

;6521
OPR0301820

;6532
Sharo, open that door there.

;6584
SRK0300850

;6595
Ah, ok.

;6611
OPR0301830

;6622
The thickness of the wall is strange right?

;6653
SRK0300860

;6664
Now that you mention it, it might be a bit thick.

;6717
OPR0301840

;6728
And how about the shape of the room?

;6771
SRK0300870

;6782
Eh?

;6789
OPR0301850

;6800
If we're going to assume the post was real,
the shape should be square...

;6874
SRK0300880

;6885
Ah...!

;6895
SRK0300890

;6906
It's not square... it's bit of a rectangle...

;6977
OPR0301860

;6988
Right, so we have...

;7007
OPR0301870

;7018
A room that looks like it should have
a layout like a square from the outside.

;7095
OPR0301880

;7106
But in reality is like this.

;7140
OPR0301890

;7151
The walls are made thick
to hide the real post and...

;7209
OPR0301900

;7220
A fake post is put up.

;7251
KMT0300230

;7262
A hidden room...

;7281
KMT0300240

;7292
So there's something there then?

;7335
OPR0301910

;7346
Probably...

;7365
ERI0300480

;7376
Kya!

;7386
AKT0300250

;7397
Wah!

;7410
ZGT0300070

;7421
*Cough*! *Cough cough*!

;7449
SRK0300900

;7460
Ah... Stairs...

;7485
NER0300780

;7496
Wow...

;7512
TYM0300070

;7523
There really is one～

;7554
OPR0301920

;7565
Anyway let's try going down.

;7599
KMT0300250

;7610
Right...

;7629
SRK0300910

;7640
Ehh～!!

;7656
NER0300790

;7667
Are we going to come this far and not go down?

;7723
SRK0300920

;7734
No...

;7747
AKT0300260

;7758
Hey... is this...

;7792
HSG0300120

;7803
A maze...?

;7816
ZGT0300080

;7827
You're kidding me～
Seems like it'll be pain to find the goal...

;7898
TYM0300080

;7909
So annoying～

;7937
NER0300800

;7948
Hey hey, the people that made this maze
must've gone through the right way...

;8046
NER0300810

;8057
Can you figure out the way with
your sense of smell, Cordelia?

;8109
CDR0300970

;8120
It's filled with the smell of
mould and dust, so I can't.

;8181
NER0300820

;8192
I see...

;8211
OPR0301930

;8222
We should be alright with this type of maze.

;8271
KMT0300260

;8282
Using the wall, heh.

;8298
OPR0301940

;8309
Yeah, it's a bit like brute-forcing,
but we should end up
going through all the routes.

;8386
OPR0301950

;8397
Let's go.

;8410
ERI0300490

;8421
Auu...!

;8437
NER0300830

;8448
Ow, ow! You're going to break my hand!

;8494
NER0300835

;8505
Don't hold so hard!

;8545
ERI0300500

;8556
S-Sorry...!

;8578
@妖刀の呪い

